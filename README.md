Proyecto realizado para la UEA de Laboratorio Temático IV

Creado por: 
Andrea Domínguez Lara
Alberto Nieto Rocha
Abraham Mendoza
Rebeca de la Luz Elizalde
Arantxa Garayzar Cristerna

Especificaciones
Unity version: 2019.313f1
Target Platform: Android
Unity Hub: 2.3.2

Para compilar a Android es necesario Instalar el paquete de Android al descargar la versión de Unity. 
