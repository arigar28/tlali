﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class pausa : MonoBehaviour
{

    bool activo;
    Canvas canvas;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    // Activar/desactivar pausa, cambiar el reloj del juego para que se pause todo.
    void Update()
    {

    }

    public void pausar(){
        //if(Input.GetKeyDown("space")){
            activo = !activo;
            canvas.enabled = activo;
            Time.timeScale = (activo) ? 0 : 1f;
        //}
    }

    public void salir(string nombreEscena){
        SceneManager.LoadScene(nombreEscena);
    }

    public void continuar(){
        activo = !activo;
        canvas.enabled = activo;
        Time.timeScale = (activo) ? 0 : 1f;
    }


}
